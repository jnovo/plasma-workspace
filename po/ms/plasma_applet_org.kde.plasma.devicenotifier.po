# plasma_applet_devicenotifier Bahasa Melayu (Malay) (ms)
# Copyright (C) 2008, 2009 K Desktop Environment
# This file is distributed under the same license as the plasma_applet_devicenotifier package.
#
# Sharuzzaman Ahmat Raslan <sharuzzaman@myrealbox.com>, 2008, 2009.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_devicenotifier\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-01 00:48+0000\n"
"PO-Revision-Date: 2009-06-02 00:19+0800\n"
"Last-Translator: Sharuzzaman Ahmat Raslan <sharuzzaman@myrealbox.com>\n"
"Language-Team: Malay <kedidiemas@yahoogroups.com>\n"
"Language: ms\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=1;\n"
"X-Generator: KBabel 1.11.4\n"

#: package/contents/ui/DeviceItem.qml:185
#, kde-format
msgctxt "@info:status Free disk space"
msgid "%1 free of %2"
msgstr ""

#: package/contents/ui/DeviceItem.qml:189
#, kde-format
msgctxt ""
"Accessing is a less technical word for Mounting; translation should be short "
"and mean 'Currently mounting this device'"
msgid "Accessing…"
msgstr ""

#: package/contents/ui/DeviceItem.qml:192
#, kde-format
msgctxt ""
"Removing is a less technical word for Unmounting; translation should be "
"short and mean 'Currently unmounting this device'"
msgid "Removing…"
msgstr ""

#: package/contents/ui/DeviceItem.qml:195
#, kde-format
msgid "Don't unplug yet! Files are still being transferred..."
msgstr ""

#: package/contents/ui/DeviceItem.qml:226
#, kde-format
msgid "Open in File Manager"
msgstr ""

#: package/contents/ui/DeviceItem.qml:229
#, kde-format
msgid "Mount and Open"
msgstr ""

#: package/contents/ui/DeviceItem.qml:231
#, kde-format
msgid "Eject"
msgstr ""

#: package/contents/ui/DeviceItem.qml:233
#, kde-format
msgid "Safely remove"
msgstr ""

#: package/contents/ui/DeviceItem.qml:275
#, kde-format
msgid "Mount"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:41
#: package/contents/ui/main.qml:238
#, kde-format
msgid "Remove All"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:42
#, fuzzy, kde-format
#| msgid "1 action for this device"
#| msgid_plural "%1 actions for this device"
msgid "Click to safely remove all devices"
msgstr "1 tindakan untuk peranti ini"

#: package/contents/ui/FullRepresentation.qml:186
#, fuzzy, kde-format
#| msgid "Last plugged in device: %1"
msgid "No removable devices attached"
msgstr "Peranti terakhir dipasang: %1"

#: package/contents/ui/FullRepresentation.qml:186
#, fuzzy, kde-format
#| msgid "No devices plugged in"
msgid "No disks available"
msgstr "Tiada peranti di plug masuk"

#: package/contents/ui/main.qml:47
#, fuzzy, kde-format
#| msgid "Last plugged in device: %1"
msgid "Most Recent Device"
msgstr "Peranti terakhir dipasang: %1"

#: package/contents/ui/main.qml:47
#, kde-format
msgid "No Devices Available"
msgstr ""

#: package/contents/ui/main.qml:245
#, kde-format
msgid "Removable Devices"
msgstr ""

#: package/contents/ui/main.qml:251
#, fuzzy, kde-format
#| msgid "Last plugged in device: %1"
msgid "Non Removable Devices"
msgstr "Peranti terakhir dipasang: %1"

#: package/contents/ui/main.qml:257
#, kde-format
msgid "All Devices"
msgstr ""

#: package/contents/ui/main.qml:265
#, fuzzy, kde-format
#| msgid "No devices plugged in"
msgid "Show popup when new device is plugged in"
msgstr "Tiada peranti di plug masuk"

#: package/contents/ui/main.qml:274
#, fuzzy, kde-format
#| msgid "Last plugged in device: %1"
msgctxt "Open auto mounter kcm"
msgid "Configure Removable Devices…"
msgstr "Peranti terakhir dipasang: %1"

#, fuzzy
#~| msgid "1 action for this device"
#~| msgid_plural "%1 actions for this device"
#~ msgid "It is currently safe to remove this device."
#~ msgstr "1 tindakan untuk peranti ini"

#, fuzzy
#~| msgid "1 action for this device"
#~| msgid_plural "%1 actions for this device"
#~ msgid "1 action for this device"
#~ msgid_plural "%1 actions for this device"
#~ msgstr[0] "1 tindakan untuk peranti ini"
#~ msgstr[1] "1 tindakan untuk peranti ini"

#, fuzzy
#~| msgid "1 action for this device"
#~| msgid_plural "%1 actions for this device"
#~ msgid "Click to eject this disc."
#~ msgstr "1 tindakan untuk peranti ini"

#, fuzzy
#~| msgid "1 action for this device"
#~| msgid_plural "%1 actions for this device"
#~ msgid "Click to safely remove this device."
#~ msgstr "1 tindakan untuk peranti ini"

#, fuzzy
#~| msgid "1 action for this device"
#~| msgid_plural "%1 actions for this device"
#~ msgid "Click to mount this device."
#~ msgstr "1 tindakan untuk peranti ini"

#, fuzzy
#~| msgid ""
#~| "Could not unmount the device.\n"
#~| "One or more files on this device are open within an application."
#~ msgid ""
#~ "Could not unmount device %1.\n"
#~ "One or more files on this device are open within an application."
#~ msgstr ""
#~ "Tidak dapat menyahlekap peranti.\n"
#~ "Satu atau lebih fail pada peranti ini terbuka didalam aplikasi."

#~ msgid ""
#~ "Cannot eject the disc.\n"
#~ "One or more files on this disc are open within an application."
#~ msgstr ""
#~ "Tidak dapat melentingkan cakera.\n"
#~ "Satu atau lebih fail dalam cakera ini terbuka didalam aplikasi."

#, fuzzy
#~| msgid "Last plugged in device: %1"
#~ msgid "Could not mount device %1."
#~ msgstr "Peranti terakhir dipasang: %1"

#, fuzzy
#~| msgid "<font color=\"%1\">Devices recently plugged in:</font>"
#~ msgid "Devices recently plugged in:"
#~ msgstr "<font color=\"%1\">Peranti dipasang baru-baru ini:</font>"
