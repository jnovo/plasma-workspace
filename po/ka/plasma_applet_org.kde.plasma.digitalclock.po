# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-05 01:00+0000\n"
"PO-Revision-Date: 2023-01-27 07:14+0100\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.2.2\n"

#: package/contents/config/config.qml:19
#, kde-format
msgid "Appearance"
msgstr "გარემოს იერსახე"

#: package/contents/config/config.qml:24
#, kde-format
msgid "Calendar"
msgstr "კალენდარი"

#: package/contents/config/config.qml:29
#: package/contents/ui/CalendarView.qml:431
#, kde-format
msgid "Time Zones"
msgstr "დროის სარტყლები"

#: package/contents/ui/CalendarView.qml:110
#, kde-format
msgid "Events"
msgstr "მოვლენები"

#: package/contents/ui/CalendarView.qml:118
#, kde-format
msgctxt "@action:button Add event"
msgid "Add…"
msgstr "დამატება…"

#: package/contents/ui/CalendarView.qml:122
#, kde-format
msgctxt "@info:tooltip"
msgid "Add a new event"
msgstr "ახალი მოვლენისს დამატება"

#: package/contents/ui/CalendarView.qml:397
#, kde-format
msgid "No events for today"
msgstr "დღეს არაფერი ხდება"

#: package/contents/ui/CalendarView.qml:398
#, kde-format
msgid "No events for this day"
msgstr "ამ დღეს არაფერი ხდება"

#: package/contents/ui/CalendarView.qml:440
#, kde-format
msgid "Switch…"
msgstr "გადართვა…"

#: package/contents/ui/CalendarView.qml:441
#: package/contents/ui/CalendarView.qml:444
#, kde-format
msgid "Switch to another timezone"
msgstr "დროის სხვა სარტყელზე გადართვა"

#: package/contents/ui/configAppearance.qml:51
#, kde-format
msgid "Information:"
msgstr "ინფორმაცია:"

#: package/contents/ui/configAppearance.qml:55
#, kde-format
msgid "Show date"
msgstr "თარიღის ჩვენება"

#: package/contents/ui/configAppearance.qml:63
#, kde-format
msgid "Adaptive location"
msgstr "ადაპტიური მდებარეობა"

#: package/contents/ui/configAppearance.qml:64
#, kde-format
msgid "Always beside time"
msgstr "დროისთვის მიყოლა"

#: package/contents/ui/configAppearance.qml:65
#, kde-format
msgid "Always below time"
msgstr "დროისთვის მიდევნება"

#: package/contents/ui/configAppearance.qml:73
#, kde-format
msgid "Show seconds:"
msgstr "წამების ჩვენება:"

#: package/contents/ui/configAppearance.qml:75
#, kde-format
msgctxt "@option:check"
msgid "Never"
msgstr "არასდროს"

#: package/contents/ui/configAppearance.qml:76
#, kde-format
msgctxt "@option:check"
msgid "Only in the tooltip"
msgstr "მხოლოდ მინიშნებაში"

#: package/contents/ui/configAppearance.qml:77
#: package/contents/ui/configAppearance.qml:97
#, kde-format
msgid "Always"
msgstr "ყოველთვის"

#: package/contents/ui/configAppearance.qml:87
#, kde-format
msgid "Show time zone:"
msgstr "დროის სარტყლის ჩვენება:"

#: package/contents/ui/configAppearance.qml:92
#, kde-format
msgid "Only when different from local time zone"
msgstr "მხოლოდ როცა განსხვავდება დროის ლოკალური სარტყლისგან"

#: package/contents/ui/configAppearance.qml:106
#, kde-format
msgid "Display time zone as:"
msgstr "დროის სარტყლის ჩვენება:"

#: package/contents/ui/configAppearance.qml:111
#, kde-format
msgid "Code"
msgstr "კოდი"

#: package/contents/ui/configAppearance.qml:112
#, kde-format
msgid "City"
msgstr "ქალაქი"

#: package/contents/ui/configAppearance.qml:113
#, kde-format
msgid "Offset from UTC time"
msgstr "UTC დროიდან წანაცვლება"

#: package/contents/ui/configAppearance.qml:125
#, kde-format
msgid "Time display:"
msgstr "დროის ჩვენება:"

#: package/contents/ui/configAppearance.qml:130
#, kde-format
msgid "12-Hour"
msgstr "12-სთ"

#: package/contents/ui/configAppearance.qml:131
#: package/contents/ui/configCalendar.qml:58
#, kde-format
msgid "Use Region Defaults"
msgstr "რეგიონის ნაგულისხმები მნიშვნელობების გამოყენება"

#: package/contents/ui/configAppearance.qml:132
#, kde-format
msgid "24-Hour"
msgstr "24-სთ"

#: package/contents/ui/configAppearance.qml:139
#, kde-format
msgid "Change Regional Settings…"
msgstr "რეგიონალური პარამეტრების შეცვლა…"

#: package/contents/ui/configAppearance.qml:150
#, kde-format
msgid "Date format:"
msgstr "თარიღის ფორმატი:"

#: package/contents/ui/configAppearance.qml:158
#, kde-format
msgid "Long Date"
msgstr "გრძელი თარიღი"

#: package/contents/ui/configAppearance.qml:163
#, kde-format
msgid "Short Date"
msgstr "მოკლე თარიღი"

#: package/contents/ui/configAppearance.qml:168
#, kde-format
msgid "ISO Date"
msgstr "ISO თარიღი"

#: package/contents/ui/configAppearance.qml:173
#, kde-format
msgctxt "custom date format"
msgid "Custom"
msgstr "ხელით მითითებული"

#: package/contents/ui/configAppearance.qml:204
#, kde-format
msgid ""
"<a href=\"https://doc.qt.io/qt-5/qml-qtqml-qt.html#formatDateTime-method"
"\">Time Format Documentation</a>"
msgstr ""
"<a href=\"https://doc.qt.io/qt-5/qml-qtqml-qt.html#formatDateTime-method"
"\">დროის ფორმატის დოკუმენტაცია</a>"

#: package/contents/ui/configAppearance.qml:228
#, kde-format
msgctxt "@label:group"
msgid "Text display:"
msgstr "ტექსტის ჩვენება:"

#: package/contents/ui/configAppearance.qml:230
#, kde-format
msgctxt "@option:radio"
msgid "Automatic"
msgstr "ავტომატური"

#: package/contents/ui/configAppearance.qml:234
#, kde-format
msgctxt "@label"
msgid ""
"Text will follow the system font and expand to fill the available space."
msgstr ""
"ტექსტი მიჰყვება სისტემურ ფონტს და გაფართოვდება მანამ, სანამ ხელმისაწვდომ "
"სივრცეს არ შეავსებს."

#: package/contents/ui/configAppearance.qml:243
#, kde-format
msgctxt "@option:radio setting for manually configuring the font settings"
msgid "Manual"
msgstr "ხელით"

#: package/contents/ui/configAppearance.qml:253
#, kde-format
msgctxt "@action:button"
msgid "Choose Style…"
msgstr "სტილის არჩევა…"

#: package/contents/ui/configAppearance.qml:266
#, kde-format
msgctxt "@info %1 is the font size, %2 is the font family"
msgid "%1pt %2"
msgstr "%1წრტ %2"

#: package/contents/ui/configAppearance.qml:277
#, kde-format
msgctxt "@title:window"
msgid "Choose a Font"
msgstr "აირჩიეთ ფონტი"

#: package/contents/ui/configCalendar.qml:44
#, kde-format
msgid "General:"
msgstr "ზოგადი:"

#: package/contents/ui/configCalendar.qml:45
#, kde-format
msgid "Show week numbers"
msgstr "კვირის რიცხვის ჩვენება"

#: package/contents/ui/configCalendar.qml:50
#, kde-format
msgid "First day of week:"
msgstr "კვირის პირველი დღე:"

#: package/contents/ui/configCalendar.qml:73
#, kde-format
msgid "Available Plugins:"
msgstr "ხელმისაწვდომი დამატებები:"

#: package/contents/ui/configTimeZones.qml:39
#, kde-format
msgid ""
"Tip: if you travel frequently, add another entry for your home time zone to "
"this list. It will only appear when you change the systemwide time zone to "
"something else."
msgstr ""
"მინიშნება: თუ ხშირად მოგზაურობთ, დაამატეთ ახალი ველი თქვენი სახლის დროის "
"სარტყელისთვის. ის მხოლოდ მაშინ გამოჩნდება, როცა მთელი სისტემის დროის "
"სარტყელს რამე სხვა მნიშვნელობაზე გადართავთ."

#: package/contents/ui/configTimeZones.qml:101
#, kde-format
msgid "Clock is currently using this time zone"
msgstr "ამჟამად საათი დროის ამ სარტყელს იყენებს"

#: package/contents/ui/configTimeZones.qml:103
#, kde-format
msgctxt ""
"@label This list item shows a time zone city name that is identical to the "
"local time zone's city, and will be hidden in the timezone display in the "
"plasmoid's popup"
msgid "Hidden while this is the local time zone's city"
msgstr "დამალულია, რადგან ეს ლოკალური დროის სარტყელი ქალაქია"

#: package/contents/ui/configTimeZones.qml:117
#, kde-format
msgid "Switch Systemwide Time Zone…"
msgstr "დროის სარტყელის მთელი სისტემისთვის შეცვლა…"

#: package/contents/ui/configTimeZones.qml:126
#, kde-format
msgid "Remove this time zone"
msgstr "დროის ამ სარტყლის წაშლა"

#: package/contents/ui/configTimeZones.qml:135
#, kde-format
msgid "Systemwide Time Zone"
msgstr "მთელი სისტემის დროის სარტყელი"

#: package/contents/ui/configTimeZones.qml:135
#, kde-format
msgid "Additional Time Zones"
msgstr "დროის დამატებითი სარტყლები"

#: package/contents/ui/configTimeZones.qml:148
#, kde-format
msgid ""
"Add more time zones to display all of them in the applet's pop-up, or use "
"one of them for the clock itself"
msgstr ""
"დაამატეთ დროის მეტი საჩვენებელი სარტყელი ან გამოიყენეთ ერთ-ერთი მათგანი, "
"როგორც საათი"

#: package/contents/ui/configTimeZones.qml:155
#, kde-format
msgid "Add Time Zones…"
msgstr "დროის სარტყლის დამატება…"

#: package/contents/ui/configTimeZones.qml:165
#, kde-format
msgid "Switch displayed time zone by scrolling over clock applet"
msgstr ""
"ნაჩვენები დროის სარტყლების შეცვლა თაგუნას ბორბლის საათზე დატრიალებით "
"შეგიძლიათ"

#: package/contents/ui/configTimeZones.qml:172
#, kde-format
msgid ""
"Using this feature does not change the systemwide time zone. When you "
"travel, switch the systemwide time zone instead."
msgstr ""
"დაიმახსოვრეთ, რომ ამ ფუნქციის გამოყენება არ ცვლის სისტემის ლოკალურ დროის "
"სარტყელს. მგზავრობისას გადაერთეთ დროის ლოკალურ სარტყელზე."

#: package/contents/ui/configTimeZones.qml:198
#, kde-format
msgid "Add More Timezones"
msgstr "დროის მეტი სარტყლის დამატება"

#: package/contents/ui/configTimeZones.qml:209
#, kde-format
msgid ""
"At least one time zone needs to be enabled. Your local timezone was enabled "
"automatically."
msgstr ""
"საჭიროა, ჩართული იყოს დროის ერთი სარტყელი მაინც. თქვენი ლოკალური დროის "
"სარტყელი ავტომატურად ჩართულია."

#: package/contents/ui/configTimeZones.qml:234
#, kde-format
msgid "%1, %2 (%3)"
msgstr "%1, %2 (%3)"

#: package/contents/ui/configTimeZones.qml:234
#, kde-format
msgid "%1, %2"
msgstr "%1, %2"

#: package/contents/ui/main.qml:133
#, kde-format
msgid "Copy to Clipboard"
msgstr "გაცვლის ბუფერში კოპირება"

#: package/contents/ui/main.qml:138
#, kde-format
msgid "Adjust Date and Time…"
msgstr "დროისა და თარიღის გასწორება…"

#: package/contents/ui/main.qml:141
#, kde-format
msgid "Set Time Format…"
msgstr "დროის ფორმატის დაყენება…"

#: package/contents/ui/Tooltip.qml:32
#, kde-format
msgctxt "@info:tooltip %1 is a localized long date"
msgid "Today is %1"
msgstr "დღეს %1-ა"

#: package/contents/ui/Tooltip.qml:110
#, kde-format
msgctxt "@label %1 is a city or time zone name"
msgid "%1:"
msgstr "%1:"

#: plugin/clipboardmenu.cpp:110
#, kde-format
msgid "Other Calendars"
msgstr "სხვა კალენდრები"

#: plugin/clipboardmenu.cpp:118
#, kde-format
msgctxt "unix timestamp (seconds since 1.1.1970)"
msgid "%1 (UNIX Time)"
msgstr "%1 (UNIX-ის დრო)"

#: plugin/clipboardmenu.cpp:121
#, kde-format
msgctxt "for astronomers (days and decimals since ~7000 years ago)"
msgid "%1 (Julian Date)"
msgstr "%1 (იულიანეს კალენდარი)"

#: plugin/timezonemodel.cpp:141
#, kde-format
msgctxt "This means \"Local Timezone\""
msgid "Local"
msgstr "ლოკალური"

#: plugin/timezonemodel.cpp:143
#, kde-format
msgid "System's local time zone"
msgstr "სისტემის ლოკალური დროის სარტყელი"

#~ msgctxt "Format: month year"
#~ msgid "%1 %2"
#~ msgstr "%1 %2"

#~ msgid "Keep Open"
#~ msgstr "ღიად დატოვება"

#~ msgid "Days"
#~ msgstr "დღე"

#~ msgid "Months"
#~ msgstr "თვე"

#~ msgid "Years"
#~ msgstr "წელი"

#~ msgid "Previous month"
#~ msgstr "წინა თვე"

#~ msgid "Previous year"
#~ msgstr "წინა წელი"

#~ msgid "Previous decade"
#~ msgstr "წინა ათწლეული"

#~ msgctxt "Reset calendar to today"
#~ msgid "Today"
#~ msgstr "დღეს"

#~ msgid "Reset calendar to today"
#~ msgstr "კალენდრის დღევანდელ დღეზე გადართვა"

#~ msgid "Next month"
#~ msgstr "შემდეგი თვე"

#~ msgid "Next year"
#~ msgstr "გაისად"

#~ msgid "Next decade"
#~ msgstr "შემდეგ ათწლეულში"
