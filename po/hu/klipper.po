#
# Tamas Szanto <tszanto@interware.hu>, 2000.
# Arpad Biro <biro_arpad@yahoo.com>, 2004.
# Kristóf Kiszel <ulysses@kubuntu.org>, 2010, 2011, 2012, 2014, 2019.
# Balázs Úr <urbalazs@gmail.com>, 2012.
# Kiszel Kristóf <kiszel.kristof@gmail.com>, 2017, 2020, 2021.
# Kristof Kiszel <ulysses@fsf.hu>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: KDE 4.2\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-17 01:00+0000\n"
"PO-Revision-Date: 2022-02-01 11:32+0100\n"
"Last-Translator: Kristof Kiszel <ulysses@fsf.hu>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.07.70\n"

#: configdialog.cpp:80
#, kde-format
msgid "Selection and Clipboard:"
msgstr "Kijelölés és vágólap:"

#: configdialog.cpp:87
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"When text or an area of the screen is highlighted with the mouse or "
"keyboard, this is the <emphasis>selection</emphasis>. It can be pasted using "
"the middle mouse button.<nl/><nl/>If the selection is explicitly copied "
"using a <interface>Copy</interface> or <interface>Cut</interface> action, it "
"is saved to the <emphasis>clipboard</emphasis>. It can be pasted using a "
"<interface>Paste</interface> action. <nl/><nl/>When turned on this option "
"keeps the selection and the clipboard the same, so that any selection is "
"immediately available to paste by any means. If it is turned off, the "
"selection may still be saved in the clipboard history (subject to the "
"options below), but it can only be pasted using the middle mouse button."
msgstr ""
"Amikor szöveget vagy a képernyő egy területét az egérrel vagy a "
"billentyűzettel kiemeli, az a <emphasis>kijelölés</emphasis>. Beilleszthető "
"a a középső egérgombbal.<nl/><nl/>Ha a kijelölés kifejezetten másolásra "
"kerül <interface>Másolás</interface> vagy <interface>Kivágás</interface> "
"művelet segítségével, akkor az elmentésre kerül a <emphasis>vágólap</"
"emphasis>ra. Beilleszthető a <interface>Beillesztés</interface> művelet "
"segítségével. <nl/><nl/>Az opció bekapcsolásával a kijelölés és a vágólap "
"azonos marad, így bármilyen kijelölés azonnal elérhető bármilyen eszközzel "
"történő beillesztéshez. Ha ki van kapcsolva, akkor a a kijelölés továbbra is "
"elmenthető a vágólap előzményeiben (a beállításoktól függően), de "
"beilleszteni csak az egér középső gombjával lehet."

#: configdialog.cpp:106
#, fuzzy, kde-format
#| msgid "Clipboard history"
msgid "Clipboard history:"
msgstr "Vágólapnapló"

#: configdialog.cpp:112
#, fuzzy, kde-format
#| msgctxt "NUmber of entries"
#| msgid " entry"
#| msgid_plural " entries"
msgctxt "Number of entries"
msgid " entry"
msgid_plural " entries"
msgstr[0] " bejegyzés"
msgstr[1] " bejegyzés"

#: configdialog.cpp:131 configdialog.cpp:169
#, kde-format
msgid "Always save in history"
msgstr "Mindig mentse az előzmények közé"

#: configdialog.cpp:135
#, kde-format
msgid "Text selection:"
msgstr "Szövegkijelölés:"

#: configdialog.cpp:137 configdialog.cpp:175
#, kde-format
msgid "Only when explicitly copied"
msgstr "Csak kifejezetten másoláskor"

#: configdialog.cpp:142
#, kde-format
msgid "Whether text selections are saved in the clipboard history."
msgstr "A szövegkijelölés mentésre kerüljön-e a vágólap előzményeibe."

#: configdialog.cpp:173
#, kde-format
msgid "Non-text selection:"
msgstr "Nem szöveges kijelölés:"

#: configdialog.cpp:180
#, kde-format
msgid "Never save in history"
msgstr "Soha nem mentse az előzmények közé"

#: configdialog.cpp:185
#, kde-format
msgid ""
"Whether non-text selections (such as images) are saved in the clipboard "
"history."
msgstr ""
"A nem szöveges kijelölés (például képek) mentésre kerüljön-e a vágólap "
"előzményeibe."

#: configdialog.cpp:250
#, fuzzy, kde-format
#| msgid "Automatic action popup time:"
msgid "Show action popup menu:"
msgstr "Automatikus műveletmenü időkorlátja:"

#: configdialog.cpp:260
#, kde-kuit-format
msgctxt "@info"
msgid ""
"When text that matches an action pattern is selected or is chosen from the "
"clipboard history, automatically show the popup menu with applicable "
"actions. If the automatic menu is turned off here, or it is not shown for an "
"excluded window, then it can be shown by using the <shortcut>%1</shortcut> "
"key shortcut."
msgstr ""

#: configdialog.cpp:269
#, kde-format
msgid "Exclude Windows..."
msgstr ""

#: configdialog.cpp:283
#, kde-format
msgctxt "Unit of time"
msgid " second"
msgid_plural " seconds"
msgstr[0] " másodperc"
msgstr[1] " másodperc"

#: configdialog.cpp:284
#, kde-format
msgctxt "No timeout"
msgid "None"
msgstr "Nincs"

#: configdialog.cpp:293
#, fuzzy, kde-format
#| msgctxt "Actions Config"
#| msgid "Actions"
msgid "Options:"
msgstr "Műveletek"

#: configdialog.cpp:320
#, kde-format
msgid "Exclude Windows"
msgstr ""

#: configdialog.cpp:350
#, kde-kuit-format
msgctxt "@info"
msgid ""
"When a <interface>match pattern</interface> matches the clipboard contents, "
"its <interface>commands</interface> appear in the Klipper popup menu and can "
"be executed."
msgstr ""

#: configdialog.cpp:359
#, kde-format
msgctxt "@title:column"
msgid "Match pattern and commands"
msgstr ""

#: configdialog.cpp:359
#, fuzzy, kde-format
#| msgid "Description"
msgctxt "@title:column"
msgid "Description"
msgstr "Leírás"

#: configdialog.cpp:365
#, kde-format
msgid "Add Action..."
msgstr "Művelet hozzáadása…"

#: configdialog.cpp:369
#, kde-format
msgid "Edit Action..."
msgstr "Művelet szerkesztése…"

#: configdialog.cpp:374
#, kde-format
msgid "Delete Action"
msgstr "Művelet törlése"

#: configdialog.cpp:381
#, kde-kuit-format
msgctxt "@info"
msgid ""
"These actions appear in the popup menu which can be configured on the "
"<interface>Action Menu</interface> page."
msgstr ""

#: configdialog.cpp:565
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Delete the selected action <resource>%1</resource><nl/>and all of its "
"commands?"
msgstr ""

#: configdialog.cpp:566
#, fuzzy, kde-format
#| msgid "Delete Action"
msgid "Confirm Delete Action"
msgstr "Művelet törlése"

#: configdialog.cpp:595
#, kde-format
msgctxt "General Config"
msgid "General"
msgstr "Általános"

#: configdialog.cpp:595
#, kde-format
msgid "General Configuration"
msgstr "Általános beállítások"

#: configdialog.cpp:596
#, fuzzy, kde-format
#| msgctxt "Actions Config"
#| msgid "Actions"
msgctxt "Popup Menu Config"
msgid "Action Menu"
msgstr "Műveletek"

#: configdialog.cpp:596
#, fuzzy, kde-format
#| msgctxt "Actions Config"
#| msgid "Actions"
msgid "Action Menu"
msgstr "Műveletek"

#: configdialog.cpp:597
#, fuzzy, kde-format
#| msgid "Actions Configuration"
msgctxt "Actions Config"
msgid "Actions Configuration"
msgstr "A műveletek beállítása"

#: configdialog.cpp:597
#, kde-format
msgid "Actions Configuration"
msgstr "A műveletek beállítása"

#: configdialog.cpp:600
#, kde-format
msgctxt "Shortcuts Config"
msgid "Shortcuts"
msgstr "Billentyűparancsok"

#: configdialog.cpp:600
#, kde-format
msgid "Shortcuts Configuration"
msgstr "A billentyűparancsok beállítása"

#: configdialog.cpp:680
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The action popup will not be shown automatically for these windows, even if "
"it is enabled. This is because, for example, a web browser may highlight a "
"URL in the address bar while typing, so the menu would show for every "
"keystroke.<nl/><nl/>If the action menu appears unexpectedly when using a "
"particular application, then add it to this list. <link>How to find the name "
"to enter</link>."
msgstr ""

#: configdialog.cpp:693
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"The name that needs to be entered here is the WM_CLASS name of the window to "
"be excluded. To find the WM_CLASS name for a window, in another terminal "
"window enter the command:<nl/><nl/>&nbsp;&nbsp;<icode>xprop | grep WM_CLASS</"
"icode><nl/><nl/>and click on the window that you want to exclude. The first "
"name that it displays after the equal sign is the one that you need to enter."
msgstr ""

#: editactiondialog.cpp:34 editcommanddialog.cpp:89
#, kde-format
msgid "Ignore"
msgstr "Kihagyás"

#: editactiondialog.cpp:36
#, kde-format
msgid "Replace Clipboard"
msgstr "Vágólap cseréje"

#: editactiondialog.cpp:38
#, kde-format
msgid "Add to Clipboard"
msgstr "Hozzáadás a vágólaphoz"

#: editactiondialog.cpp:122
#, kde-format
msgid "Command"
msgstr "Parancs"

#: editactiondialog.cpp:124
#, kde-format
msgid "Output"
msgstr ""

#: editactiondialog.cpp:126
#, kde-format
msgid "Description"
msgstr "Leírás"

#: editactiondialog.cpp:179
#, kde-format
msgid "Action Properties"
msgstr "Művelet tulajdonságai"

#: editactiondialog.cpp:191
#, kde-kuit-format
msgctxt "@info"
msgid ""
"An action takes effect when its <interface>match pattern</interface> matches "
"the clipboard contents. When this happens, the action's <interface>commands</"
"interface> appear in the Klipper popup menu; if one of them is chosen, the "
"command is executed."
msgstr ""

#: editactiondialog.cpp:203
#, kde-format
msgid "Enter a pattern to match against the clipboard"
msgstr ""

#: editactiondialog.cpp:205
#, kde-format
msgid "Match pattern:"
msgstr ""

#: editactiondialog.cpp:208
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The match pattern is a regular expression. For more information see the "
"<link url=\"https://en.wikipedia.org/wiki/Regular_expression\">Wikipedia "
"entry</link> for this topic."
msgstr ""

#: editactiondialog.cpp:219
#, kde-format
msgid "Enter a description for the action"
msgstr ""

#: editactiondialog.cpp:220 editcommanddialog.cpp:83
#, kde-format
msgid "Description:"
msgstr "Leírás:"

#: editactiondialog.cpp:223
#, kde-format
msgid "Include in automatic popup"
msgstr ""

#: editactiondialog.cpp:227
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The commands for this match will be included in the automatic action popup, "
"if it is enabled in the <interface>Action Menu</interface> page. If this "
"option is turned off, the commands for this match will not be included in "
"the automatic popup but they will be included if the popup is activated "
"manually with the <shortcut>%1</shortcut> key shortcut."
msgstr ""

#: editactiondialog.cpp:262
#, fuzzy, kde-format
#| msgid "Add Command"
msgid "Add Command..."
msgstr "Parancs hozzáadása"

#: editactiondialog.cpp:267
#, fuzzy, kde-format
#| msgid "Add Command"
msgid "Edit Command..."
msgstr "Parancs hozzáadása"

#: editactiondialog.cpp:273
#, fuzzy, kde-format
#| msgid "Remove Command"
msgid "Delete Command"
msgstr "Parancs törlése"

#: editactiondialog.cpp:388
#, kde-kuit-format
msgctxt "@info"
msgid "Delete the selected command <resource>%1</resource>?"
msgstr ""

#: editactiondialog.cpp:389
#, kde-format
msgid "Confirm Delete Command"
msgstr ""

#: editcommanddialog.cpp:46
#, fuzzy, kde-format
#| msgid "Action Properties"
msgid "Command Properties"
msgstr "Művelet tulajdonságai"

#: editcommanddialog.cpp:59
#, kde-format
msgid "Enter the command and arguments"
msgstr ""

#: editcommanddialog.cpp:62
#, fuzzy, kde-format
#| msgid "Command"
msgid "Command:"
msgstr "Parancs"

#: editcommanddialog.cpp:71
#, kde-kuit-format
msgctxt "@info"
msgid ""
"A <placeholder>&#37;s</placeholder> in the command will be replaced by the "
"complete clipboard contents. <placeholder>&#37;0</placeholder> through "
"<placeholder>&#37;9</placeholder> will be replaced by the corresponding "
"captured texts from the match pattern."
msgstr ""

#: editcommanddialog.cpp:81
#, kde-format
msgid "Enter a description for the command"
msgstr ""

#: editcommanddialog.cpp:91
#, kde-format
msgid "Output from command:"
msgstr ""

#: editcommanddialog.cpp:93
#, fuzzy, kde-format
#| msgid "Replace Clipboard"
msgid "Replace current clipboard"
msgstr "Vágólap cseréje"

#: editcommanddialog.cpp:97
#, fuzzy, kde-format
#| msgid "Add to Clipboard"
msgid "Append to clipboard"
msgstr "Hozzáadás a vágólaphoz"

#: editcommanddialog.cpp:101
#, kde-format
msgid "What happens to the standard output of the command executed."
msgstr ""

#: editcommanddialog.cpp:115
#, kde-format
msgid "Reset the icon to the default for the command"
msgstr ""

#: editcommanddialog.cpp:121
#, kde-format
msgid "Icon:"
msgstr ""

#: historyimageitem.cpp:36
#, kde-format
msgid "%1x%2 %3bpp"
msgstr "%1x%2 %3 bpp"

#: klipper.cpp:155
#, fuzzy, kde-format
#| msgid "Automatic action popup time:"
msgctxt "@action:inmenu Toggle automatic action"
msgid "Automatic Action Popup Menu"
msgstr "Automatikus műveletmenü időkorlátja:"

#: klipper.cpp:194
#, fuzzy, kde-format
#| msgid "C&lear Clipboard History"
msgctxt "@action:inmenu"
msgid "C&lear Clipboard History"
msgstr "A műveletnapló tö&rlése"

#: klipper.cpp:201
#, fuzzy, kde-format
#| msgid "&Configure Klipper…"
msgctxt "@action:inmenu"
msgid "&Configure Klipper…"
msgstr "A Klipper &beállítása…"

#: klipper.cpp:207
#, fuzzy, kde-format
#| msgctxt "@item:inmenu Quit Klipper"
#| msgid "&Quit"
msgctxt "@action:inmenu Quit Klipper"
msgid "&Quit"
msgstr "&Kilépés"

#: klipper.cpp:212
#, fuzzy, kde-format
#| msgid "Manually Invoke Action on Current Clipboard"
msgctxt "@action:inmenu"
msgid "Manually Invoke Action on Current Clipboard"
msgstr "Művelet végrehajtása az aktuális vágólapon"

#: klipper.cpp:220
#, fuzzy, kde-format
#| msgid "&Edit Contents…"
msgctxt "@action:inmenu"
msgid "&Edit Contents…"
msgstr "Tartalom sz&erkesztése…"

#: klipper.cpp:228
#, fuzzy, kde-format
#| msgid "&Show Barcode…"
msgctxt "@action:inmenu"
msgid "&Show Barcode…"
msgstr "Vonalkód megjeleníté&se…"

#: klipper.cpp:237
#, fuzzy, kde-format
#| msgid "Next History Item"
msgctxt "@action:inmenu"
msgid "Next History Item"
msgstr "Következő előzményelem"

#: klipper.cpp:242
#, fuzzy, kde-format
#| msgid "Previous History Item"
msgctxt "@action:inmenu"
msgid "Previous History Item"
msgstr "Előző előzményelem"

#: klipper.cpp:249
#, fuzzy, kde-format
#| msgid "Open Klipper at Mouse Position"
msgctxt "@action:inmenu"
msgid "Show Items at Mouse Position"
msgstr "A Klipper megnyitása a kurzorpozíciónál"

#: klipper.cpp:260
#, fuzzy, kde-format
#| msgid "Klipper - Clipboard Tool"
msgctxt "%1 is application display name"
msgid "%1 - Clipboard Tool"
msgstr "Klipper vágólapkezelő"

#: klipper.cpp:556
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You can enable URL actions later in the <interface>Actions</interface> page "
"of the Clipboard applet's configuration window"
msgstr ""
"Az URL-műveleteket később is engedélyezheti a vágólap kisalkalmazás "
"beállítóablakának <interface>Műveletek</interface> oldalán."

#: klipper.cpp:595
#, kde-format
msgid "Should Klipper start automatically when you login?"
msgstr "Automatikusan elinduljon a program?"

#: klipper.cpp:596
#, kde-format
msgid "Automatically Start Klipper?"
msgstr "Elinduljon automatikusan a program?"

#: klipper.cpp:597
#, kde-format
msgid "Start"
msgstr "Elindítás"

#: klipper.cpp:598
#, kde-format
msgid "Do Not Start"
msgstr "Nem kell elindítani"

#: klipper.cpp:945
#, kde-format
msgid "Edit Contents"
msgstr "A tartalom szerkesztése"

#: klipper.cpp:1012
#, kde-format
msgid "Mobile Barcode"
msgstr "Mobil vonalkód"

#: klipper.cpp:1059
#, fuzzy, kde-format
#| msgid "Really delete entire clipboard history?"
msgid "Do you really want to clear and delete the entire clipboard history?"
msgstr "Biztosan törli a teljes vágólapnaplót?"

#: klipper.cpp:1060
#, fuzzy, kde-format
#| msgid "C&lear Clipboard History"
msgid "Clear Clipboard History"
msgstr "A műveletnapló tö&rlése"

#: klipper.cpp:1076 klipper.cpp:1085
#, kde-format
msgid "Clipboard history"
msgstr "Vágólapnapló"

#: klipper.cpp:1102
#, kde-format
msgid "up"
msgstr "fel"

#: klipper.cpp:1109
#, kde-format
msgid "current"
msgstr "jelenlegi"

#: klipper.cpp:1116
#, kde-format
msgid "down"
msgstr "le"

#. i18n: ectx: label, entry (Version), group (General)
#: klipper.kcfg:10
#, kde-format
msgid "Klipper version"
msgstr "Klipper verzió"

#. i18n: ectx: label, entry (KeepClipboardContents), group (General)
#: klipper.kcfg:13
#, fuzzy, kde-format
#| msgid "Save the clipboard history across desktop sessions"
msgid "Save history across desktop sessions"
msgstr "A vágólap előzményeinek mentése az asztali munkamenetek között"

#. i18n: ectx: tooltip, entry (KeepClipboardContents), group (General)
#: klipper.kcfg:15
#, kde-format
msgid ""
"Retain the clipboard history, so it will be available the next time you log "
"in."
msgstr ""
"A vágólap előzményeinek megőrzése, hogy a következő bejelentkezéskor is "
"elérhetőek legyenek."

#. i18n: ectx: label, entry (PreventEmptyClipboard), group (General)
#: klipper.kcfg:18
#, kde-format
msgid "Prevent the clipboard from being cleared"
msgstr "A vágólap törlésének megelőzése"

#. i18n: ectx: whatsthis, entry (PreventEmptyClipboard), group (General)
#: klipper.kcfg:20
#, kde-format
msgid ""
"Do not allow the clipboard to be cleared, for example when an application "
"exits."
msgstr "Ne engedje a vágólap kiürítését, például alkalmazások kilépésekor."

#. i18n: ectx: label, entry (SyncClipboards), group (General)
#: klipper.kcfg:27
#, kde-format
msgid "Keep the selection and clipboard the same"
msgstr "A kijelölés és a vágólap egyezzen meg"

#. i18n: ectx: whatsthis, entry (SyncClipboards), group (General)
#: klipper.kcfg:29
#, kde-format
msgid ""
"Content selected with the cursor is automatically copied to the clipboard so "
"that it can be pasted with either a Paste action or a middle-click.<br/><a "
"href=\"1\">More about the selection and clipboard</a>."
msgstr ""
"A kurzorral kijelölt tartalom automatikusan a vágólapra másolódik, így "
"beilleszthető lesz a Beillesztés művelettel vagy középső gombos kattintással."
"<br/><a href=\"1\">További információk a kijelölésről és a vágólapról.</a>"

#. i18n: ectx: label, entry (IgnoreSelection), group (General)
#: klipper.kcfg:32
#, kde-format
msgid "Ignore the selection"
msgstr "Kijelölés mellőzése"

#. i18n: ectx: whatsthis, entry (IgnoreSelection), group (General)
#: klipper.kcfg:34
#, kde-format
msgid ""
"Content selected with the cursor but not explicitly copied to the clipboard "
"is not automatically stored in the clipboard history, and can only be pasted "
"using a middle-click."
msgstr ""
"A kurzorral kijelölt, de a vágólapra nem kifejezetten másolt tartalom nem "
"kerül be a vágólap előzményei közé, és csak a középső egérgombbal "
"illeszthető be."

#. i18n: ectx: label, entry (SelectionTextOnly), group (General)
#: klipper.kcfg:37
#, kde-format
msgid "Text selection only"
msgstr "Csak szövegkijelölés"

#. i18n: ectx: whatsthis, entry (SelectionTextOnly), group (General)
#: klipper.kcfg:39
#, kde-format
msgid ""
"Only store text selections in the clipboard history, not images or any other "
"type of data."
msgstr ""
"Csak a szövegkijelölések tárolódjanak az előzményekben, képek vagy más "
"adatok ne."

#. i18n: ectx: label, entry (IgnoreImages), group (General)
#: klipper.kcfg:42
#, kde-format
msgid "Always ignore images"
msgstr "Mindig mellőzze a képeket"

#. i18n: ectx: whatsthis, entry (IgnoreImages), group (General)
#: klipper.kcfg:44
#, kde-format
msgid ""
"Do not store images in the clipboard history, even if explicitly copied."
msgstr ""
"Ne tároljon képeket a vágólap előzményeiben, még kifejezett másoláskor sem."

#. i18n: ectx: label, entry (UseGUIRegExpEditor), group (General)
#: klipper.kcfg:47
#, kde-format
msgid "Use graphical regexp editor"
msgstr "Grafikus reguláriskifejezés-szerkesztő használata"

#. i18n: ectx: label, entry (URLGrabberEnabled), group (General)
#: klipper.kcfg:51
#, fuzzy, kde-format
#| msgid "Ignore the selection"
msgid "Immediately on selection"
msgstr "Kijelölés mellőzése"

#. i18n: ectx: tooltip, entry (URLGrabberEnabled), group (General)
#: klipper.kcfg:52
#, kde-format
msgid ""
"Show the popup menu of applicable actions as soon as a selection is made."
msgstr ""

#. i18n: ectx: label, entry (NoActionsForWM_CLASS), group (General)
#: klipper.kcfg:57
#, kde-format
msgid "No actions for WM_CLASS"
msgstr "Nincsenek műveletek a WM_CLASS-hoz"

#. i18n: ectx: label, entry (TimeoutForActionPopups), group (General)
#: klipper.kcfg:60
#, fuzzy, kde-format
#| msgid "Automatic action popup time:"
msgid "Automatic action menu time:"
msgstr "Automatikus műveletmenü időkorlátja:"

#. i18n: ectx: tooltip, entry (TimeoutForActionPopups), group (General)
#: klipper.kcfg:64
#, fuzzy, kde-format
#| msgid "Display the automatic action popup for this time."
msgid "Display the automatic action popup menu for this time."
msgstr "Az automatikus műveletmenü megjelenítése ennyi ideig."

#. i18n: ectx: label, entry (MaxClipItems), group (General)
#: klipper.kcfg:67
#, fuzzy, kde-format
#| msgid "Clipboard history size:"
msgid "History size:"
msgstr "A vágólapnapló mérete:"

#. i18n: ectx: tooltip, entry (MaxClipItems), group (General)
#: klipper.kcfg:71
#, kde-format
msgid "The clipboard history will store up to this many items."
msgstr "A vágólap előzményei legfeljebb ennyi elemet tárolnak."

#. i18n: ectx: label, entry (ActionList), group (General)
#: klipper.kcfg:74
#, kde-format
msgid "Dummy entry for indicating changes in an action's tree widget"
msgstr "Üres bejegyzés egy művelet faeleme változásainak jelzésére"

#. i18n: ectx: label, entry (StripWhiteSpace), group (Actions)
#: klipper.kcfg:84
#, fuzzy, kde-format
#| msgid "Strip whitespace when executing an action"
msgid "Trim whitespace from selection"
msgstr "Üres helyek eltávolítása művelet végrehajtásakor"

#. i18n: ectx: whatsthis, entry (StripWhiteSpace), group (Actions)
#: klipper.kcfg:86
#, fuzzy, kde-format
#| msgid ""
#| "Remove white space from the start and end of clipboard text before "
#| "performing an action. For example, this ensures that the URL pasted in a "
#| "browser is interpreted as expected.<br/><br/>The text saved on the "
#| "clipboard is not affected."
msgid ""
"Remove any whitespace from the start and end of selected text, before "
"performing an action. For example, this ensures that a URL pasted in a "
"browser is interpreted as expected. The text saved on the clipboard is not "
"affected."
msgstr ""
"Üres helyek eltávolítása a vágólap szövegeinek elejéről és végéről művelet "
"végrehajtása előtt. Például URL-cím másolásakor ez segít abban, hogy a "
"böngésző megfelelően értelmezze azt.<br/><br/>A vágólapra mentett szöveget "
"nem érinti."

#. i18n: ectx: label, entry (ReplayActionInHistory), group (Actions)
#: klipper.kcfg:89
#, fuzzy, kde-format
#| msgid "Replay actions on an item selected from history"
msgid "For an item chosen from history"
msgstr "Műveletek engedélyezése naplóbeli elemeken"

#. i18n: ectx: tooltip, entry (ReplayActionInHistory), group (Actions)
#: klipper.kcfg:91
#, fuzzy, kde-format
#| msgid "Whether text selections are saved in the clipboard history."
msgid ""
"Show the popup menu of applicable actions if an entry is chosen from the "
"clipboard history."
msgstr "A szövegkijelölés mentésre kerüljön-e a vágólap előzményeibe."

#. i18n: ectx: label, entry (EnableMagicMimeActions), group (Actions)
#: klipper.kcfg:94
#, fuzzy, kde-format
#| msgid "Enable MIME-based actions"
msgid "Include MIME actions"
msgstr "MIME-típus alapú műveletek bekapcsolása"

#. i18n: ectx: whatsthis, entry (EnableMagicMimeActions), group (Actions)
#: klipper.kcfg:96
#, fuzzy, kde-format
#| msgid ""
#| "If this option is set, and a file name or URL is selected, include "
#| "applications that can handle its MIME type in the actions popup."
msgid ""
"If a file name or URL is selected, include applications that can accept its "
"MIME type in the popup menu."
msgstr ""
"Ha ez az opció be van állítva, és fájlnevet vagy URL-t választanak ki, a "
"műveletmenü tartalmazza a MIME-típust kezelő alkalmazásokat is."

#: klipperpopup.cpp:105
#, fuzzy, kde-format
#| msgid "Clipboard Contents"
msgctxt "%1 is application display name"
msgid "%1 - Clipboard Items"
msgstr "A vágólap tartalma"

#: klipperpopup.cpp:109
#, kde-format
msgid "Search…"
msgstr "Keresés…"

#: klipperpopup.cpp:167
#, fuzzy, kde-format
#| msgid "Regular expression:"
msgid "Invalid regular expression, %1"
msgstr "Reguláris kifejezés:"

#: klipperpopup.cpp:172 tray.cpp:25 tray.cpp:53
#, kde-format
msgid "Clipboard is empty"
msgstr "A vágólap üres"

#: klipperpopup.cpp:174
#, kde-format
msgid "No matches"
msgstr "Nincs találat"

#: main.cpp:27 tray.cpp:22
#, kde-format
msgid "Klipper"
msgstr "Klipper"

#: main.cpp:29
#, fuzzy, kde-format
#| msgid "KDE cut & paste history utility"
msgid "Plasma cut & paste history utility"
msgstr "KDE vágólapkezelő segédprogram"

#: main.cpp:31
#, kde-format
msgid ""
"(c) 1998, Andrew Stanley-Jones\n"
"1998-2002, Carsten Pfeiffer\n"
"2001, Patrick Dubroy"
msgstr ""
"(C) Andrew Stanley-Jones, 1998.\n"
"Carsten Pfeiffer, 1998-2002.\n"
"Patrick Dubroy, 2001."

#: main.cpp:34
#, kde-format
msgid "Carsten Pfeiffer"
msgstr "Carsten Pfeiffer"

#: main.cpp:34
#, kde-format
msgid "Author"
msgstr "Szerző"

#: main.cpp:36
#, kde-format
msgid "Andrew Stanley-Jones"
msgstr "Andrew Stanley-Jones"

#: main.cpp:36
#, kde-format
msgid "Original Author"
msgstr "Az eredeti program szerzője"

#: main.cpp:38
#, kde-format
msgid "Patrick Dubroy"
msgstr "Patrick Dubroy"

#: main.cpp:38
#, kde-format
msgid "Contributor"
msgstr "Közreműködők"

#: main.cpp:40
#, kde-format
msgid "Luboš Luňák"
msgstr "Luboš Luňák"

#: main.cpp:40
#, kde-format
msgid "Bugfixes and optimizations"
msgstr "Hibajavítások, optimalizálások"

#: main.cpp:42
#, kde-format
msgid "Esben Mose Hansen"
msgstr "Esben Mose Hansen"

#: main.cpp:42
#, kde-format
msgid "Previous Maintainer"
msgstr "Előző karbantartó"

#: main.cpp:44
#, kde-format
msgid "Martin Gräßlin"
msgstr "Martin Gräßlin"

#: main.cpp:44
#, kde-format
msgid "Maintainer"
msgstr "Karbantartó"

#: main.cpp:46
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Kiszel Kristóf,Bíró Árpád,Szántó Tamás"

#: main.cpp:46
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ulysses@kubuntu.org,biro_arpad@yahoo.com,tszanto@interware.hu"

#: popupproxy.cpp:145
#, kde-format
msgid "&More"
msgstr "&Továbbiak"

#: tray.cpp:25
#, kde-format
msgid "Clipboard Contents"
msgstr "A vágólap tartalma"

#: urlgrabber.cpp:199
#, kde-format
msgid "Disable This Popup"
msgstr "Ez az ablak ne jelenjen meg többé"

#: urlgrabber.cpp:205
#, kde-format
msgid "&Cancel"
msgstr "&Mégsem"

#~ msgid "Delete clipboard history?"
#~ msgstr "Törli a vágólapnaplót?"

#~ msgid "Action list:"
#~ msgstr "Műveletlista:"

#~ msgid "Regular Expression"
#~ msgstr "Reguláris kifejezés"

#~ msgid ""
#~ "Click on a highlighted item's column to change it. \"%s\" in a command "
#~ "will be replaced with the clipboard contents.<br>For more information "
#~ "about regular expressions, you could have a look at the <a href=\"https://"
#~ "en.wikipedia.org/wiki/Regular_expression\">Wikipedia entry about this "
#~ "topic</a>."
#~ msgstr ""
#~ "Kattintson a kiemelt elem oszlopára a megváltoztatásához. A parancsban "
#~ "lévő „%s” a vágólaptartalommal lesz helyettesítve.<br>Ha többet szeretne "
#~ "tudni a reguláris kifejezésekről, látogasson el ide: <a href=\"https://en."
#~ "wikipedia.org/wiki/Regular_expression\">Wikipedia-bejegyzés a reguláris "
#~ "kifejezésekről</a>."

#~ msgid "Output Handling"
#~ msgstr "Kimenetkezelés"

#~ msgid "new command"
#~ msgstr "új parancs"

#~ msgid "Command Description"
#~ msgstr "Parancs leírása"

#~ msgid "Action properties:"
#~ msgstr "Művelet tulajdonságai:"

#~ msgid "Automatic:"
#~ msgstr "Automatikus:"

#~ msgid "List of commands for this action:"
#~ msgstr "A művelethez tartozó parancsok listája:"

#~ msgid "Double-click an item to edit"
#~ msgstr "Kattintson kétszer egy elem szerkesztéséhez"

#~ msgid "Remove whitespace when executing actions"
#~ msgstr "Művelet végrehajtásakor az üres karakterek eltávolítása"

#~ msgid "Advanced..."
#~ msgstr "Speciális…"

#~ msgid "Advanced Settings"
#~ msgstr "Speciális beállítások"

#~ msgid "D&isable Actions for Windows of Type WM_CLASS"
#~ msgstr "A műveletek letiltása a következő WM_CLASS értékű abl&akoknál:"

#~ msgid ""
#~ "<qt>This lets you specify windows in which Klipper should not invoke "
#~ "\"actions\". Use<br /><br /><center><b>xprop | grep WM_CLASS</b></"
#~ "center><br />in a terminal to find out the WM_CLASS of a window. Next, "
#~ "click on the window you want to examine. The first string it outputs "
#~ "after the equal sign is the one you need to enter here.</qt>"
#~ msgstr ""
#~ "<qt>Ez lehetővé teszi olyan ablakosztályok felsorolását, amelyeknél a "
#~ "Klipper nem hajthat végre \"műveleteket\". Használja az <br /><br /"
#~ "><center><b>xprop | grep WM_CLASS</b></center><br />parancsot egy "
#~ "parancsértelmezőben, ha meg szeretné tudni egy ablak WM_CLASS értékét. "
#~ "Ezután kattintson a megvizsgálandó ablakra. Az egyenlőségjel után először "
#~ "kiírt sztringet kell itt beírni.</qt>"

#~ msgid "Enable Clipboard Actions"
#~ msgstr "Vágólapműveletek bekapcsolása"

#~ msgid "URL grabber enabled"
#~ msgstr "URL-letöltő bekapcsolása"

#~ msgid "Replay action in history"
#~ msgstr "Művelet újra végrehajtása a vágólapnaplón"

#~ msgid ""
#~ "When a clipboard item is selected from the history popup, automatically "
#~ "perform the configured actions on it."
#~ msgstr ""
#~ "Ha egy elemet kiválasztanak az előzmények felugró ablakában, "
#~ "automatikusan hajtódjanak végre a beállított műveletek azon."

#~ msgid "Save clipboard contents on exit"
#~ msgstr "A vágólap tartalmának mentése kilépéskor"

#~ msgid "Synchronize contents of the clipboard and the selection"
#~ msgstr "A vágólaptartalom és a kijelölés szinkronizálása"

#~ msgid "Keep clipboard contents"
#~ msgstr "A vágólaptartalom megtartása"

#~ msgid ""
#~ "Selecting this option has the effect, that the clipboard can never be "
#~ "emptied. E.g. when an application exits, the clipboard would usually be "
#~ "emptied."
#~ msgstr ""
#~ "Ha bejelöli ezt az opciót, akkor a vágólap soha nem üríthető ki. Például "
#~ "alkalmazás kilépésekor általában megtörténik a vágólap kiürítése is. "

#~ msgid "Ignore Selection"
#~ msgstr "A kijelölések figyelmen kívül hagyása"

#~ msgid ""
#~ "When an area of the screen is selected with mouse or keyboard, this is "
#~ "called \"the selection\".<br/>If this option is set, the selection is not "
#~ "entered into the clipboard history, though it is still available for "
#~ "pasting using the middle mouse button."
#~ msgstr ""
#~ "Kijelölésnek nevezik azt, amikor a képernyő egy területe egérrel vagy "
#~ "billentyűzettel ki van jelölve.<br/>Ha ez a beállítás be van kapcsolva, a "
#~ "kijelölés nem kerül a vágólapra, de középső egérgombbal beilleszthető "
#~ "marad."

#~ msgid "Synchronize clipboard and selection"
#~ msgstr "A vágólap és kijelölés szinkronizálása"

#~ msgid ""
#~ "When an area of the screen is selected with mouse or keyboard, this is "
#~ "called \"the selection\".<br/>If this option is selected, the selection "
#~ "and the clipboard is kept the same, so that anything in the selection is "
#~ "immediately available for pasting elsewhere using any method, including "
#~ "the traditional middle mouse button. Otherwise, the selection is recorded "
#~ "in the clipboard history, but the selection can only be pasted using the "
#~ "middle mouse button. Also see the 'Ignore Selection' option."
#~ msgstr ""
#~ "Kijelölésnek nevezik azt, amikor a képernyő egy területe egérrel vagy "
#~ "billentyűzettel ki van jelölve.<br/>Ha ez a beállítás be van kapcsolva, a "
#~ "kijelölés és a vágólap tartalma meg fog egyezni, így bármilyen kijelölés "
#~ "azonnal beilleszthető lesz bárhová bármilyen módszerrel, beleértve a "
#~ "hagyományos középső egérgombos beillesztést is. Egyébként a kijelölés "
#~ "rögzítve lesz a vágólapelőzményekben, de csak a középső egérgombbal lesz "
#~ "beilleszthető. Lásd „A kijelölések figyelmen kívül hagyása” beállítást."

#~ msgid "Selection text only"
#~ msgstr "Csak szövegkijelölés"

#~ msgid ""
#~ "When an area of the screen is selected with mouse or keyboard, this is "
#~ "called \"the selection\".<br/>If this option is selected, only text "
#~ "selections are stored in the history, while images and other selections "
#~ "are not."
#~ msgstr ""
#~ "Kijelölésnek nevezik azt, amikor a képernyő egy területe egérrel vagy "
#~ "billentyűzettel ki van jelölve.<br/>Ha ez a beállítás be van kapcsolva, "
#~ "csak a szövegkijelölések lesznek eltárolva az előzményekben, a képek és "
#~ "egyéb kijelölések nem."

#~ msgid "Timeout for action popups (seconds)"
#~ msgstr "A műveletmenük lát&hatósági ideje másodpercekben"

#~ msgid "A value of 0 disables the timeout"
#~ msgstr "0 érték esetén végtelen lesz a várakozási idő"

#~ msgid "Clipboard history size"
#~ msgstr "A vágólapnapló mérete"

#~ msgid ""
#~ "Sometimes, the selected text has some whitespace at the end, which, if "
#~ "loaded as URL in a browser would cause an error. Enabling this option "
#~ "removes any whitespace at the beginning or end of the selected string "
#~ "(the original clipboard contents will not be modified)."
#~ msgstr ""
#~ "Előfordulhat, hogy a kijelölt szöveg végén üres karakterek találhatók. "
#~ "Ilyenkor, ha a szöveget URL-ként kell értelmezni, ez hibajelzéshez vezet. "
#~ "Ha ez az opció be van jelölve, akkor a program a kijelölt szöveg elejéről "
#~ "és végéről eltávolítja az üres karaktereket (a vágólap tartalma nem "
#~ "változik meg)."

#~ msgid "%1 - Actions For: %2"
#~ msgstr "%1 - Műveletek: %2"

#~ msgid "&Edit Contents..."
#~ msgstr "A t&artalom szerkesztése..."

#~ msgid "<empty clipboard>"
#~ msgstr "<üres vágólap>"

#~ msgid ""
#~ "You can enable URL actions later by left-clicking on the Klipper icon and "
#~ "selecting 'Enable Clipboard Actions'"
#~ msgstr ""
#~ "Ha az URL műveleteket később engedélyezni szeretné, kattintson a bal "
#~ "gombbal a program ikonjára és válassza a „Vágólapműveletek bekapcsolása” "
#~ "menüpontot"

#~ msgid "Enable Clipboard &Actions"
#~ msgstr "Vágólapműveletek bek&apcsolása"

#~ msgid "Show Klipper Popup-Menu"
#~ msgstr "A felbukkanó menü megjelenítése"

#, fuzzy
#~| msgid "&Popup menu at mouse-cursor position"
#~ msgid "Popup menu at mouse-cursor position"
#~ msgstr "Felbukkanó menü az egérm&utatónál"

#~ msgid "Clipboard/Selection Behavior"
#~ msgstr "A vágólapi kijelölések hatása"

#~ msgid "Separate clipboard and selection"
#~ msgstr "A két vágólap tartalma maradjon független"

#~ msgid ""
#~ "This option prevents the selection being recorded in the clipboard "
#~ "history. Only explicit clipboard changes are recorded."
#~ msgstr ""
#~ "Ennek hatására a vágólapi kijelölések nem lesznek naplózva, csak az "
#~ "adatmódosító műveletek."

#~ msgid ""
#~ "<qt>There are two different clipboard buffers available:<br /><br /"
#~ "><b>Clipboard</b> is filled by selecting something and pressing Ctrl+C, "
#~ "or by clicking \"Copy\" in a toolbar or menubar.<br /><br /><b>Selection</"
#~ "b> is available immediately after selecting some text. The only way to "
#~ "access the selection is to press the middle mouse button.<br /><br />You "
#~ "can configure the relationship between Clipboard and Selection.</qt>"
#~ msgstr ""
#~ "<qt>Két különböző vágólap létezik a rendszerben:<br /><br />Az egyik a "
#~ "<b>normál vágólap</b>, melyet egy rész kijelölése után a Ctrl+C "
#~ "megnyomásával, a \"Másolás\" menüponttal vagy eszköztárgombbal lehet "
#~ "feltölteni.<br /><br />A másik a <b>kijelölési vágólap</b>, amelyet a KDE "
#~ "automatikusan feltölt egy szövegrész kijelölésekor. Ez utóbbi tartalmát "
#~ "csak egy módon, a középső egérgombbal lehet elérni.<br /><br />A két "
#~ "vágólap közötti kapcsolat szabályozható.</qt>"

#~ msgid "Klipper - clipboard tool"
#~ msgstr "Klipper - vágólapkezelő eszköz"

#~ msgid "Selecting this option synchronizes these two buffers."
#~ msgstr "Ha ezt bejelöli, a két vágólap tartalma mindig szinkronban lesz."

#~ msgid ""
#~ "Using this option will only set the selection when highlighting something "
#~ "and the clipboard when choosing e.g. \"Copy\" in a menubar."
#~ msgstr ""
#~ "Ennek hatására másolás és kivágás esetén csak a normál vágólap, kijelölés "
#~ "esetén csak a kijelölési vágólap tartalma fog változni."

#~ msgid "Action &list (right click to add/remove commands):"
#~ msgstr ""
#~ "Műveletl&ista (a jobb egérgombbal kattintva lehet parancsot hozzáadni "
#~ "vagy törölni)"

#~ msgid "&Use graphical editor for editing regular expressions"
#~ msgstr "&Grafikus szerkesztőprogram használata reguláris kifejezésekhez"

#~ msgid ""
#~ "Click on a highlighted item's column to change it. \"%s\" in a command "
#~ "will be replaced with the clipboard contents."
#~ msgstr ""
#~ "Kattintson egy kijelölt elem valamelyik oszlopára, ha módosítani "
#~ "szeretné. A \"%s\" szöveg helyére a vágólap tartalma fog kerülni."

#~ msgid "Double-click here to set the command to be executed"
#~ msgstr "Kattintson ide duplán a parancs beállításához"

#~ msgid "Double-click here to set the regular expression"
#~ msgstr "Kattintson ide duplán a reguláris kifejezés beállításához"

#~ msgid "<new action>"
#~ msgstr "<új művelet>"

#~ msgid "Enable &Actions"
#~ msgstr "A műveletek &engedélyezése"
